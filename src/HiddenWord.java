public class HiddenWord {
    private String word;

    public HiddenWord(String answer) {
        this.word = answer;
    }

    public String getHint(String guess) {
        String out = "";
        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == this.word.charAt(i)) {
                out = out + guess.charAt(i);
            } else if (this.word.indexOf(guess.charAt(i)) > -1) {
                out = out + "+";
            } else {
                out = out + "*";
            }
        }
        return out;
    }
}
